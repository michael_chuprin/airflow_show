# Purpose of this folder

`[work/]` The working directory, from my SAS days, was used to store temporary data files. I like having the main console code directly in the project folder outside of the sub-folders, and storing intermediate data that I may want to reference later in the work sub-folder.

Idea gotten from [here](https://medium.com/human-in-a-machine-world/folder-structure-for-data-analysis-62a84949a6ce)