# Purpose of this folder

`[result/]` Contains the final results and explanatory markdown files.

Idea gotten from [here](https://medium.com/human-in-a-machine-world/folder-structure-for-data-analysis-62a84949a6ce)