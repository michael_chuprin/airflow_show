from datetime import datetime
from string import ascii_letters

from os.path import dirname as dn
from os.path import join as jn
from os import walk
from os.path import basename

from airflow import DAG
from airflow.operators.python_operator import PythonOperator

def get_data_folder():
    project_root = dn(dn(__file__))
    data_folder = jn(project_root, 'data')

    return data_folder

def get_all_files_in_folder(folderpath):
    
    filepaths = []
    for root, dirs, files in walk(folderpath):
        for file in files:
            fp = jn(root, file)
            filepaths.append(fp)

    filepaths = sorted(filepaths)

    return filepaths

def read_file(filepath):
    with open(filepath, 'r') as f:
        return ' '.join(f.read().splitlines())

def calc_min_and_max_numbers(base_name):
    src_path = jn(get_data_folder(), 'input', base_name)
    dst_path = jn(get_data_folder(), 'result', f"{base_name[:-4]}_mmn.txt")

    file_contents = read_file(src_path)

    only_numbers = []
    for x in file_contents:
        if x in '012345679':
            only_numbers.append(x)
        else:
            only_numbers.append(' ')

    numbers_as_ints = [int(x) for x in ''.join(only_numbers).split()]
    minimum_number  = min(numbers_as_ints)
    maximum_number  = max(numbers_as_ints)

    with open(dst_path, 'w') as f:
        f.write(f"minimum number is {minimum_number}\n")
        f.write(f"maximum number is {maximum_number}\n")
    

def calc_shorted_and_longest_sequences(base_name):
    src_path = jn(get_data_folder(), 'input', base_name)
    dst_path = jn(get_data_folder(), 'result', f"{base_name[:-4]}_sls.txt")

    file_contents = read_file(src_path)

    only_letters = []
    for x in file_contents:
        if x in ascii_letters:
            only_letters.append(x)
        else:
            only_letters.append(' ')


    shortest_sequence = ''
    longest_sequence = ''

    sequences = ''.join(only_letters).split()

    for sequence in sequences:

        # if shortest_sequence is currently '', then set it to the current sequence 
        if len(sequence) > 0:
            if not shortest_sequence:
                shortest_sequence = sequence

        if len(sequence) < len(shortest_sequence):
            shortest_sequence = sequence

        if len(sequence) > len(longest_sequence):
            longest_sequence = sequence

    with open(dst_path, 'w') as f:
        f.write(f"shortest sequence is {shortest_sequence}\n")
        f.write(f"longest sequence is {longest_sequence}\n")

def create_dag(dag_name, schedule_interval, op_kwargs, default_args):

    base_name = op_kwargs['base_name']
    
    with DAG(dag_name, schedule_interval=schedule_interval, default_args=default_args) as dag:

        
        t1 = PythonOperator(task_id         = 'calc_min_and_max_numbers',
                            python_callable = calc_min_and_max_numbers,
                            op_kwargs       = {'base_name': base_name})

        t2 = PythonOperator(task_id         = 'calc_shorted_and_longest_sequences',
                            python_callable = calc_shorted_and_longest_sequences,
                            op_kwargs       = {'base_name': base_name})

        t1 >> t2
                            

    return dag



input_folder    = jn(get_data_folder(), 'input')
all_input_files = get_all_files_in_folder(input_folder)

for idx, input_file in enumerate(all_input_files):

    base_name = basename(input_file)
    dag_name  = 'DAG_for_{}'.format(base_name[:-4])

    globals()[dag_name] = create_dag(dag_name          = dag_name,
                                     schedule_interval = '@daily',
                                     op_kwargs         = {'base_name': base_name},
                                     default_args      = {'owner': 'airflow', 'start_date': datetime(2018, 1, 1)})