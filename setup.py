from setuptools import setup, find_packages


setup(
    name='Template',
    version='0.0',
    packages=find_packages(),
    long_description=open('README.md').read(),
)
