1. [Airflow Show](#airflow-show)
2. [How it works](#how-it-works)
3. [UI Links](#ui-links)
4. [Prerequisites](#prerequisites)
5. [Setup](#setup)
6. [Using the Code](#using-the-code)


# Airflow Show

The name of this project is **Airflow Show** and its purpose is to display competancy with [Apache Airflow](https://airflow.apache.org/)

# How it works 

This project will scan `data/input/` for `txt` files, calculate statistics for each file, and dump those statistics to a file by the same name into `data/result/`.

Calculated statistics will be:

* min and max numbers seen
* shortest and longest sequence of letters

# UI Links

* Airflow: [localhost:8080](localhost:8080)
* Flower: [localhost:5555](localhost:5555)

# Prerequisites

Docker

#  Setup

1. Clone this project
2. `cd` into the project
3. Run  
 
    $ docker-compose up -d

# Using the Code

1. In `data/input/`, make a new `.txt` file and call it whatever you want. For example, `foobar.txt`
2. Open it and fill it with random chars, preferably using [this](https://gph.is/1xRQ4d3) method
3. Save it
4. Go to the Airflow UI at [localhost:8080](localhost:8080) and turn on your DAG
5. Wait 5 minutes
6. Go to `data/result/` and view the airflow generated file